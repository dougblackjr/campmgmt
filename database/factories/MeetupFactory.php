<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Meetup;
use App\Program;
use Faker\Generator as Faker;

$factory->define(Meetup::class, function (Faker $faker) {
    return [
        'program_id' => factory(Program::class)->create(),
        'title' => $faker->name,
        'description' => $faker->text(),
    ];
});
