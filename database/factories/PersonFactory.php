<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
    ];
});

$factory->afterCreatingState(Person::class, 'camper', function ($person, $faker) {
    $person->assignRole('camper');
});

$factory->afterCreatingState(Person::class, 'guardian', function ($person, $faker) {
    $person->assignRole('guardian');
});
