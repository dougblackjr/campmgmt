<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Program;
use App\User;
use Faker\Generator as Faker;

$factory->define(Program::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'active' => 1,
        'user_id' => factory(User::class)->create(),
    ];
});
