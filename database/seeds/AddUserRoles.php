<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class AddUserRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$roles = [
    		'admin',
    		'leader',
    		'guardian',
    		'counselor',
    		'camper',
    	];

    	foreach ($roles as $r) {

    		$newRole = Role::create(['name' => $r]);

    	}

    	$user = User::create([
    		'name' => 'Doug Black',
    		'email' => 'dougblackjr@gmail.com',
    		'password' => Hash::make('password'),
    	]);

        $user->assignRole('admin');

    }
}
