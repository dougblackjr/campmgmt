<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserProfile extends Model
{

	use SoftDeletes;

	protected $table = 'user_profiles';

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

	protected $fillable = [
		'user_id',
		'stripe_id',
		'card_brand',
		'card_last_four',
		'active',
		'needs_no_subs'
	];

	public function user() {

		return $this->belongsTo(User::class);

	}

}
