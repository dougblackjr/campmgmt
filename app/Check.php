<?php

namespace App;

use App\Observers\CheckObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Check extends Model
{

    use SoftDeletes;

    protected $table = 'checks';

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected $fillable = [
    	'meetup_id',
    	'person_id',
    	'time',
    	'notes',
        'in_out'
    ];

    public static function booted() {

        static::observe(new CheckObserver());

    }

    public function meetup() {

    	return $this->belongsTo(Meetup::class);

    }

    public function person() {

    	return $this->belongsTo(Person::class);

    }

    public function isCheckedIn() {

        return in_array($this->in_out, ['1', 1]);

    }

    public function isCheckedOut() {

        return ! $this->inCheckedIn();

    }

}
