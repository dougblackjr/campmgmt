<?php

namespace App\Console\Commands;

use App\Jobs\StartMeetup as StartMeetupJob;
use App\Meetup;
use Illuminate\Console\Command;

class StartScheduleMeetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meetup:start-scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates initial out checks for scheduled meetup';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $meetups = Meetup::with('program', 'program.people')
                        ->whereDate('start', now()->format('Y-m-d H:i:00'))
                        ->get();

        $this->info('Got ' . $meetups->count() . ' meetups');

        $meetups->each(static function($meetup) {
            StartMeetupJob::dispatchNow($meetup);
        });

        return 0;
    }
}
