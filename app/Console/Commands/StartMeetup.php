<?php

namespace App\Console\Commands;

use App\Meetup;
use App\Jobs\StartMeetup as StartMeetupJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StartMeetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meetup:start {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates initial out checks for meetup';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Starting meetup chill');

        $this->info('Working with meetup ' . $id);

        $meetup = Meetup::with('program', 'program.people')
                    ->where('id', $id)
                    ->first();

        StartMeetupJob::dispatchNow($meetup);

        return 0;

    }

}
