<?php

namespace App\Jobs;

use App\Check;
use App\Meetup;
use App\Person;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StartMeetup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Meetup model.
     *
     * @var \App\Meetup
     */
    public $meetup;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Meetup $meetup)
    {
        $this->meetup = $meetup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Person::where('program_id', '=', $this->meetup->program_id)
            ->cursor()
            ->filter(static function ($person) {
                return $person->hasRole('camper');
            })->each(function ($person) {
                $this->meetup->checkIn($person, now(), 'Initial');
            });
    }
}
