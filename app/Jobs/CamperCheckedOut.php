<?php

namespace App\Jobs;

use App\Check;
use App\Notifications\CamperCheckedOutFromMeetup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CamperCheckedOut implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The check-in/out model.
     *
     * @var \App\Check
     */
    public $check;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Check $check)
    {
        $this->check = $check;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->check->loadMissing('person.guardians.profile', 'meetup');

        $guardians = $this->check->person->guardians;

        foreach ($guardians as $guardian) {
            $guardian->profile->notify(new CamperCheckedOutFromMeetup(
                 $this->check->person,
                 $this->check->meetup
            ));
        }
    }
}
