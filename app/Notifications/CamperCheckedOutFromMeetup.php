<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class CamperCheckedOutFromMeetup extends Notification
{
    use Queueable;

    /**
     * The camper profile.
     *
     * @var \App\Person
     */
    public $camper;

    /**
     * The meetup.
     *
     * @var \App\Meetup
     */
    public $meetup;

    /**
     * Create a new notification instance.
     *
     * @param \App\Person $camper
     * @param \App\Meetup $meetup
     * @return void
     */
    public function __construct($camper, $meetup)
    {
        $this->camper = $camper;
        $this->meetup = $meetup;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TwilioChannel::class];
    }

    /**
     * Get the twilio sms representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toTwilio($notifiable)
    {
        $this->meetup->loadMissing('program');

        return (new TwilioSmsMessage())->content(
            __('notifications.checked-out', [
                'camper' => $this->camper->first_name,
                'meetup' => $this->meetup->name,
                'program' => $this->meetup->program->name,
            ])
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
