<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Person extends Model
{

    use SoftDeletes, HasRoles;

    protected $table = 'people';

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected $fillable = [
    	'first_name',
    	'last_name',
    	'email',
        'image',
    	'user_id',
        'program_id',
    ];

    protected $guard_name = 'web';

    protected $with = [ 'profile' ];

    public function program() {

        return $this->belongsTo(Program::class);

    }

    public function user() {

    	return $this->belongsTo(User::class);

    }

    public function profile() {

    	return $this->hasOne(PersonProfile::class);

    }

    public function teams() {

        return $this->belongsToMany(Team::class);

    }

    public function checks() {

        return $this->hasMany(Check::class);

    }

    public function guardians() {

        return $this->belongsToMany(Person::class, 'people_people', 'relator_id', 'related_id')->role('guardian');

    }

    public function dependents() {

        return $this->belongsToMany(Person::class, 'people_people', 'related_id', 'relator_id')->role('camper');

    }

    public function getRoleAttribute() {

        return $this->getRoleNames()->first();

    }

}
