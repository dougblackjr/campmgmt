<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{

	use SoftDeletes;

	protected $table = 'subscriptions';

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

	protected $fillable = [
		'user_id',
		'name',
		'stripe_id',
		'stripe_plan',
		'quantity',
		'trial_ends_at',
		'ends_at',
		'active',
		'next_run',
	];

	public function user() {

		return $this->belongsTo(User::class);

	}

}
