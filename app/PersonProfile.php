<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class PersonProfile extends Model
{

    use Notifiable, SoftDeletes;

	protected $table = 'person_profiles';

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

	protected $fillable = [
		'person_id',
		'gender',
		'dob',
		'home_phone',
		'work_phone',
		'cell_phone',
		'school',
		'grade',
		'shirt_size',
		'allergies',
		'notes'
	];

	public function person() {

		return $this->belongsTo(Person::class);

	}

	/**
     * Get the notification routing information for the given driver.
     *
     * @param  string  $driver
     * @param  \Illuminate\Notifications\Notification|null  $notification
     * @return mixed
     */
    public function routeNotificationFor($driver, $notification = null)
    {

    	if ($driver === 'twilio') {
    		return $this->cell_phone;
    	}

    	return null;

    }

}
