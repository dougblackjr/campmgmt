<?php

namespace App;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meetup extends Model
{

    use SoftDeletes;

    protected $table = 'meetups';

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected $fillable = [
        'program_id',
        'title',
        'description',
        'start',
        'end',
    ];

    public function program() {

        return $this->belongsTo(Program::class);

    }

    public function checks() {

        return $this->hasMany(Check::class);

    }

    public function teams() {

        return $this->belongsToMany(Team::class);

    }

    public function scopeByProgram(Builder $query, Program $program): Builder {

        return $query->where('program_id', '=', $program->getKey());

    }

    /**
     * Check in person to the meetup.
     */
    public function checkIn(Person $person, CarbonInterface $datetime, string $note): Check {

        return Check::withoutEvents(function () use ($person, $datetime, $note) {
            return Check::create([
                'meetup_id' => $this->getKey(),
                'person_id' => $person->getKey(),
                'time' => $datetime,
                'notes' => $note,
                'in_out' => 0,
            ]);
        });

    }

}
