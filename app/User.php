<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasRoles;

    protected $fillable = [
        'name',
        'email',
        'password',
        'last_login_at',
        'last_login_ip',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = [ 'profile' ];

    public function profile() {

        return $this->hasOne(UserProfile::class);

    }

    public function subscriptions() {

        return $this->hasMany(Subscription::class);

    }

    public function programs() {

        return $this->hasMany(Program::class);

    }

    public function person() {

        return $this->hasOne(Person::class);

    }

    // Create pivots
    public function relator() {

        return $this->belongsToMany(User::class, 'people_people', 'related_id', 'relator_id')->withPivot();

    }

    public function related() {

        return $this->hasMany(User::class, 'people_people', 'relator_id', 'related_id')->withPivot();

    }

    public function guardians() {

        return $this->belongsToMany(User::class, 'people_people', 'related_id', 'relator_id')->withPivot()->where('relationship', 'guardian');

    }

    public function dependents() {

        return $this->belongsToMany(User::class, 'people_people', 'relator_id', 'related_id')->withPivot()->where('relationship', 'guardian');

    }

    public function getRoleAttribute() {

        return $this->getRoleNames()->first();

    }

}
