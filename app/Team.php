<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{

    use SoftDeletes;

    protected $table = 'teams';

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected $fillable = [
    	'program_id',
    	'name',
    	'active',
    ];

    public function program() {

    	return $this->belongsTo('App\Program');

    }

    public function people() {

        return $this->belongsToMany(Person::class);

    }

    public function meetups() {

        return $this->belongsToMany(Meetup::class);

    }

}
