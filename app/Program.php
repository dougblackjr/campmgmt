<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Program extends Model
{

	use SoftDeletes, HasRelationships;

	protected $table = 'programs';

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

	protected $fillable = [
		'user_id',
		'name',
		'active',
	];

	public function user() {

		return $this->belongsTo(User::class);

	}

	public function teams() {

		return $this->hasMany(Team::class);

	}

	public function meetups() {

		return $this->hasMany(Meetup::class);

	}

	public function people() {

		return $this->hasMany(Person::class);
		return $this->hasManyDeep(Person::class, [Team::class, 'person_team']);

	}

	public function scopeVisibleTo(Builder $query, User $user): Builder {

		$user->loadMissing('person.dependents');

		return $query->where(function ($query) use ($user) {
			return $query->when($user->hasRole('admin'), function ($query) use ($user) {
				return $query->where('user_id', '=', $user->getKey());
			})->when($user->hasRole('counselor'), function ($query) use ($user) {
				return $query->whereIn('id', [$user->person->program_id]);
			})->when($user->hasRole('guardian'), function ($query) use ($user) {
				return $query->whereIn('id', $user->person->dependents->pluck('program_id'));
			});
		});

	}

}
