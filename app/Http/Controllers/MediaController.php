<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use ImageOptimizer;

class MediaController extends Controller
{

	public function put(Request $request) {	

		$path = $request->file('file')->store('public/people');

		ImageOptimizer::optimize(storage_path() . '/app/' . $path, storage_path() . '/app/' . $path);

		$path = str_replace('public/', '', $path);
		
		return response()->json($path);
	}

}