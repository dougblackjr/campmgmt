<?php

namespace App\Http\Controllers;

use App\Team;
use App\Program;
use App\Person;
use Auth;
use DB;
use Illuminate\Http\Request;

class TeamController extends Controller
{

	public function index(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		if (!is_null($program)) {
			$program->load('people.roles', 'teams', 'teams.people', 'meetups', 'meetups.checks');

			$program->people->each(function($p) {

				$p->role = $p->getRoleNames()->first();

				return $p;

			});
		}

		return view('team.index', [
			'program' => $program,
			'user' => $this->userFromRequest($request),
		]);

	}

	public function show(Request $request)
	{

		$program = Program::visibleTo($request->user())->first();

		$program->load('teams', 'teams.people', 'teams.people.checks', 'meetups', 'meetups.checks');

		$program->teams->each(function($t) {

			$t->people->each(function($p) {

				$p->role = $p->getRoleNames()->first();

				return $p;

			});

			return $t;

		});

		return response()->json($program->teams);

	}

	public function get(Request $request, $id) {

		$team = Team::with('people', 'people.checks')->where('id', $id)->first();

		$team->people->map(function($p) {
			$p->role = $p->getRoleNames()->first();
		});

		if(request()->wantsJson()) {

			return response()->json($team);

		}

		return view('team.view', [
			'team' => $team,
			'user' => $this->userFromRequest($request)
		]);

	}

	public function store(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		$this->authorize('update', $program);

		if(!$program) return response()->json('Missing program', 405);

		if(!$request->name) return response()->json('Missing team name', 405);

		$team = Team::updateOrCreate(
			[
				'program_id' => $program->id,
				'id' => isset($request->id) ? $request->id : null
			],
			[
				'name' => $request->name,
				'active' => $request->active ?? true
			]
		);

		if($team) {

			return response()->json($team);

		}

	}

	public function removeFromTeam(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		$this->authorize('update', $program);

		$person = $program->people()
							->where('id', $request->person)
							->with('teams')
							->first();

		if(!$person) return response()->json('nope', 500);

		$result = DB::table('person_team')
						->where('person_id', $request->person)
						->where('team_id', $request->team)
						->delete();

		return response()->json($result);

	}

}
