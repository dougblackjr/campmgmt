<?php

namespace App\Http\Controllers;

use App\Jobs\StartMeetup as StartMeetupJob;
use App\Meetup;
use App\Program;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MeetupController extends Controller
{

    public function index(Request $request)
    {

        return view('meetup.index', [
            'user' => $this->userFromRequest($request),
        ]);

    }

    public function query(Request $request)
    {

        $program = Program::visibleTo($request->user())->first();

        $program->load('meetups', 'meetups.checks', 'meetups.checks.person', 'meetups.teams');

        return response()->json($program->meetups);

    }

    public function show(Request $request, Meetup $meetup, $latest = true)
    {

        $program = Program::visibleTo($request->user())->first();

        $meetup->load('checks', 'checks.person', 'teams');

        // Parse meetups for latest
        if($latest) {

            $meetup->checks = $this->getLatest($meetup->checks);

        } else {

            $checks = $meetup->checks()->withTrashed()->orderBy('created_at')->get();

            $meetup->history = $checks;

        }

        if(request()->wantsJson()) {

            return response()->json($meetup);

        }

        return view('meetup.view', [
            'meetup' => $meetup,
            'user' => $this->userFromRequest($request),
        ]);

    }

    public function showWithAllChecks(Meetup $meetup) {

        $meetup->load('checks', 'checks.person', 'teams');

        $checks = $meetup->checks()->with('person')->withTrashed()->orderBy('created_at')->get();

        return response()->json($checks);

    }

    public function store(Request $request)
    {

        $this->authorize('create', Meetup::class);

        $program = Program::visibleTo($request->user())->first();

        $meetup = Meetup::updateOrCreate(
            [
                'id'            => isset($request->meetup['id']) ? $request->meetup['id'] : null,
                'program_id'    => $program->id,
            ],
            [
                'title'         => $request->meetup['title'],
                'description'   => $request->meetup['description'],
                'start'         => Carbon::parse($request->meetup['start']),#, 'America/New_York'),
                'end'           => Carbon::parse($request->meetup['end']),#, 'America/New_York'),
            ]
        );

        // If it has teams, sync
        if($request->teams) {

            foreach ($request->teams as $t) {

                $meetup->teams()->attach($t);

            }

        }

        return response()->json($meetup);

    }

    public function startMeetup(Request $request, $id) {

        $meetup = Meetup::where('id', '=', $id)
            ->byProgram(Program::visibleTo($request->user())->first())
            ->first();

        if(!$meetup) return response()->json('Not found');

        $this->authorize('update', $meetup);

        StartMeetupJob::dispatchNow($meetup);

        return response()->json([]);

    }

    private function getLatest($checks) {

        // Group by person
        $checks = $checks->groupBy('person_id');

        $output = $checks->map(function($c) {

            $theCollction = $c->sortByDesc('created_at');

            return $theCollction->first();

        });

        return $output->flatten();

    }

}
