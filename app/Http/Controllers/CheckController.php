<?php

namespace App\Http\Controllers;

use App\Check;
use Illuminate\Http\Request;

class CheckController extends Controller
{

    public function store(Request $request) {

    }

    public function update(Check $check, Request $request) {

    	if($check) {

			$new = $check->replicate();

			$check->delete();

		    $new->time = now();
		    $new->notes = $request->notes ?: 'No note';
		    $new->in_out = !$new->in_out;

		    $new->save();

		    return response()->json($new);

    	}

    }

}