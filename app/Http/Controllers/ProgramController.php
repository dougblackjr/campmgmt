<?php

namespace App\Http\Controllers;

use App\Program;
use App\Team;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
	public function index(Request $request) {

		return view('program.index', [
			'user' => $this->userFromRequest($request),
		]);

	}

	public function show(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		if (!is_null($program)) {
			$program->load('people.roles', 'teams', 'teams.people', 'meetups', 'meetups.checks');

			$program->people->each(function($p) {

				$p->role = $p->getRoleNames()->first();

				return $p;

			});
		}

		return response()->json($program);

	}

	public function edit(Request $request) {

		$program = Program::find($request->id);

		$this->authorize('uprade', $program);

		if(!$program) return response()->json('Program not found', 405);

		if(!$request->name) return response()->json('Name not found', 405);

		$program->update(['name' => $request->name, 'active' => $request->active]);

		return response()->json($program);

	}

}
