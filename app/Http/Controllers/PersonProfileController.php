<?php

namespace App\Http\Controllers;

use App\PersonProfile;
use Illuminate\Http\Request;

class PersonProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonProfile  $personProfile
     * @return \Illuminate\Http\Response
     */
    public function show(PersonProfile $personProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonProfile  $personProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonProfile $personProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonProfile  $personProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PersonProfile $personProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonProfile  $personProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonProfile $personProfile)
    {
        //
    }
}
