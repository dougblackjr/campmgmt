<?php

namespace App\Http\Controllers;

use App\Meetup;
use App\Program;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        return view('home', [
            'user' => $this->userFromRequest($request),
        ]);

    }

    public function dashboard(Request $request) {

        $program = Program::with('meetups', 'people', 'teams')
            ->visibleTo($request->user())
            ->first();

        if (! is_null($program)) {
            // Get next meetup
            $meetup = Meetup::where('program_id', $program->id)
                                ->whereDate('start', '>=', now()->startOfDay())
                                ->first();

            $program->next = $meetup;
        }

        return response()->json($program);

    }

}
