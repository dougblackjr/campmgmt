<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserProfile;
use App\Subscription;
use App\Program;
use Hash;

class AccountController extends Controller
{

    public function index(Request $request)
    {

        return view('account.index', [
            'user' => $this->userFromRequest($request),
        ]);

    }

    public function show(Request $request)
    {

        $user = $request->user();

        $user->load(
            'subscriptions',
            'person.program',
            'programs.meetups',
            'programs.teams.people',
        );

        return response()->json($user);

    }

    public function updateAccount(Request $request)
    {

        $user = $request->user();

        if($request->name) {

            $user->name = $request->name;

        }

        if($request->pass) {

            $user->password = Hash::make($request->pass);

        }

        $user->save();

        return response()->json($user);

    }

    public function addProgram(Request $request) {

        $user = $request->user();

        if(!$request->name) return response()->json('Missing name', 403);

        $program = Program::create([
            'user_id'   => $user->id,
            'name'      => $request->name,
        ]);

        if($program) {

            return response()->json($program);

        }

    }


}
