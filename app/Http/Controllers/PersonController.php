<?php

namespace App\Http\Controllers;

use App\Person;
use App\PersonProfile;
use App\Program;
use Auth;
use DB;
use Illuminate\Http\Request;

class PersonController extends Controller
{

	public function index(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		if (!is_null($program)) {
			$program->load('people.roles', 'people.teams', 'people.checks');

			$program->people->each(function($p) {

				$p->role = $p->getRoleNames()->first();

				return $p;

			});
		}

		return view('person.index', [
			'program' => $program,
			'user' => $this->userFromRequest($request),
		]);

	}

	public function get(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		$people = $program->people()
							->with('teams', 'checks')
							->get();

		return response()->json($people);


	}

	public function show(Request $request, $id) {

		$program = Program::visibleTo($request->user())->first();

		$person = $program->people()
							->where('id', $id)
							->with('teams', 'checks')
							->first();

		if(!$person) return response()->json('nope');

		return response()->json($person);

	}

	public function remove(Request $request, $id) {

		$program = Program::visibleTo($request->user())->first();

		$this->authorize('update', $program);

		$person = $program->people()
							->where('id', $id)
							->with('teams', 'checks')
							->first();


		if(!$person) return response()->json('nope');

		$person->delete();

		return response()->json($person);

	}

	public function showView(Request $request, $id) {

		$program = Program::visibleTo($request->user())->first();

		$person = $program->people()
							->where('id', $id)
							->with('teams', 'checks', 'checks.meetup', 'guardians', 'dependents')
							->first();

		if(!$person) return response()->json('nope');

		$person->role = $person->getRoleNames()->first();

		return view('person.view', [
			'person' => $person,
			'user' => $this->userFromRequest($request),
		]);

	}

	public function getType(Request $request, $type) {

		$program = Program::visibleTo($request->user())->first();

		$person = $program->people()
							->with('teams', 'checks')
							->get();

		if($type != 'all') {

			$person = $person->filter(function($p) use($type) {
				return $p->getRoleNames()->first() == $type;
			});

		}

		return response()->json($person);

	}

	public function storeWithRole(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		$this->authorize('update', $program);

		if($request->id) {
			return $this->update($request);
		}

		// Adds students
		$person = Person::updateOrCreate(
			[
				'id'	=> isset($request->person['id']) ? $request->person['id'] : null,
				'program_id' => $program->id,
			],
			[
				'first_name' => $request->person['first_name'],
				'last_name' => $request->person['last_name'],
				'email' => isset($request->person['email']) ? $request->person['email'] : null,
				'image' => isset($request->person['image']) ? $request->person['image'] : null,
			]
		);

		if($request->role) {

			$person->assignRole($request->role);

		}

		$profile = PersonProfile::updateOrCreate(
			[
				'person_id' => $person->id,
			],
			[
				'gender' => isset($request->person['profile']['gender']) ? $request->person['profile']['gender'] : null,
				'home_phone' => isset($request->person['profile']['home_phone']) ? $request->person['profile']['home_phone'] : null,
				'work_phone' => isset($request->person['profile']['work_phone']) ? $request->person['profile']['work_phone'] : null,
				'cell_phone' => isset($request->person['profile']['cell_phone']) ? $request->person['profile']['cell_phone'] : null,
				'dob' => isset($request->person['profile']['dob']) ? $request->person['profile']['dob'] : null,
				'school' => isset($request->person['profile']['school']) ? $request->person['profile']['school'] : null,
				'grade' => isset($request->person['profile']['grade']) ? $request->person['profile']['grade'] : null,
				'shirt_size' => isset($request->person['profile']['shirt_size']) ? $request->person['profile']['shirt_size'] : null,
				'allergies' => isset($request->person['profile']['allergies']) ? $request->person['profile']['allergies'] : null,
				'notes' => isset($request->person['profile']['notes']) ? $request->person['profile']['notes'] : null,
			]
		);

		// If it has teams, sync
		if($request->teams) {

			foreach ($request->teams as $t) {

				$person->teams()->attach($t);

			}

		}

		if($request->dependents) {

			foreach ($request->dependents as $g) {

				$person->dependents()->attach($g);

			}

		}

		return response()->json($person->load('profile', 'teams'));

	}

	public function store(Request $request) {

		$program = Program::visibleTo($request->user())->first();

		$this->authorize('update', $program);

		// Adds students
		$person = Person::create([
			'first_name' => $request->person['first_name'],
			'last_name' => $request->person['last_name'],
			'image' => isset($request->person['image']) ? $request->person['image'] : null,
			'email' => isset($request->person['email']) ? $request->person['email'] : null,
			'program_id' => $program->id,
		]);

		$person->assignRole('camper');

		$profile = PersonProfile::create([
			'person_id' => $person->id,
			'gender' => isset($request->person['profile']['gender']) ? $request->person['profile']['gender'] : null,
			'dob' => isset($request->person['profile']['dob']) ? $request->person['profile']['dob'] : null,
			'school' => isset($request->person['profile']['school']) ? $request->person['profile']['school'] : null,
			'grade' => isset($request->person['profile']['grade']) ? $request->person['profile']['grade'] : null,
			'shirt_size' => isset($request->person['profile']['shirt_size']) ? $request->person['profile']['shirt_size'] : null,
			'allergies' => isset($request->person['profile']['allergies']) ? $request->person['profile']['allergies'] : null,
			'notes' => isset($request->person['profile']['notes']) ? $request->person['profile']['notes'] : null,
		]);

		// Sync guardians
		if($request->guardians) {

			foreach ($request->guardians as $g) {

				$person->guardians()->attach($g);

			}

		}

		// If it has teams, sync
		if($request->teams) {

			foreach ($request->teams as $t) {

				$person->teams()->attach($t);

			}

		}

		// Return
		return response()->json($person->load('profile'));

	}

}
