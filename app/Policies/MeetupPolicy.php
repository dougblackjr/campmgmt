<?php

namespace App\Policies;

use App\Meetup;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MeetupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Meetup  $meetup
     * @return mixed
     */
    public function view(User $user, Meetup $meetup)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyRole(['admin', 'counselor']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Meetup  $meetup
     * @return mixed
     */
    public function update(User $user, Meetup $meetup)
    {
        return ($user->hasRole('admin') && $meetup->program->user_id == $user->id)
            || ($user->hasRole('counselor') && $meetup->program_id == $user->person->program_id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Meetup  $meetup
     * @return mixed
     */
    public function delete(User $user, Meetup $meetup)
    {
        return ($user->hasRole('admin') && $meetup->program->user_id == $user->id)
            || ($user->hasRole('counselor') && $meetup->program_id == $user->person->program_id);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Meetup  $meetup
     * @return mixed
     */
    public function restore(User $user, Meetup $meetup)
    {
        return ($user->hasRole('admin') && $meetup->program->user_id == $user->id)
            || ($user->hasRole('counselor') && $meetup->program_id == $user->person->program_id);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Meetup  $meetup
     * @return mixed
     */
    public function forceDelete(User $user, Meetup $meetup)
    {
        return ($user->hasRole('admin') && $meetup->program->user_id == $user->id)
            || ($user->hasRole('counselor') && $meetup->program_id == $user->person->program_id);
    }
}
