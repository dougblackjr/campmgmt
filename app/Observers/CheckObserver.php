<?php

namespace App\Observers;

use App\Check;
use App\Jobs\CamperCheckedIn;
use App\Jobs\CamperCheckedOut;

class CheckObserver
{
    /**
     * Handle the check "created" event.
     *
     * @param  \App\Check  $check
     * @return void
     */
    public function created(Check $check)
    {
        if ($check->isCheckedIn()) {
            CamperCheckedIn::dispatch($check);
        } else {
            CamperCheckedOut::dispatch($check);
        }
    }

    /**
     * Handle the check "updated" event.
     *
     * @param  \App\Check  $check
     * @return void
     */
    public function updated(Check $check)
    {
        if ($check->wasChanged('in_out')) {
            if ($check->isCheckedIn()) {
                CamperCheckedIn::dispatch($check);
            } else {
                CamperCheckedOut::dispatch($check);
            }
        }
    }
}
