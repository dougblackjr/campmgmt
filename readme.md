# MIN SPECS:
1) DASHBOARD: With view of who is a student vs. chaperone, who is checked in and out
2) ADD PERSON: Add a student or chaperone with information
3) CHECK-IN/OUT: The student would check in and out with a reason.
https://youthpastorscoalition.com/team252/
https://www.jotform.com/youthpastorscoalition/team252-19

# IDEAS:
Make a SAAS? (CampWise. Planning Center. Brightwell. RegPack. Sawyer. Amilia. CampDoc.)

# PACKAGES:
Spatie permissions (composer require spatie/laravel-permission)

# ROLES:
admin
leader
guardian
counselor
camper

# MODELS:
User - standard
UserProfile
Subscription
Program (has many Teams)
Team (belongs to Program, has many Person/students)
Person (both counselor, camper, belongsToMany Meetups, belongsToMany Attendance)
Meetup (belongs to Program, hasMany Person)
RegistrationForm (belongs to Meetup, hasMany RegistrationFormField)
RegistrationFormField (belongs to RegistrationFormField)
Registration (belongs to Person, belongs to Meetup)
Check (belongs to Person)
Payment (belongs to Registration)

## UserProfile:
stripe_id
card_brand
card_last_four
active

## Subscription:
user_id
name
stripe_id
stripe_plan
quantity
trial_ends_at
ends_at
active
next_run

## Program:
name
active

## Team:
name
active

## Person:
first_name
last_name
email
user_id

## PersonProfile:
gender
dob
home_phone
work_phone
cell_phone
school
grade
shirt_size

## Persons_Teams

## Meetup
program_id
title
description
start
end

## Check
meetup_id
person_id
time
notes