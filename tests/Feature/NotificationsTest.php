<?php

namespace Tests\Feature;

use App\Check;
use App\Meetup;
use App\Notifications\CamperCheckedInToMeetup;
use App\Notifications\CamperCheckedOutFromMeetup;
use App\Person;
use App\PersonProfile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use NotificationChannels\Twilio\TwilioChannel;
use Spatie\TestTime\TestTime;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed')->run();
    }

    /** @test */
    public function it_can_translate_checked_in_language_string()
    {
        $this->assertSame(
            'Tom has checked-in to Laracon 2020 (Laravel)',
            __('notifications.checked-in', ['camper' => 'Tom', 'meetup' => 'Laracon 2020', 'program' => 'Laravel'])
        );
    }

    /** @test */
    public function it_can_generate_notification_for_a_checked_in()
    {
        Notification::fake();
        TestTime::freeze();

        $meetup = factory(Meetup::class)->create([
            'start' => now(),
            'end' => now()->addHours(2),
        ]);
        $camper = factory(Person::class)->states('camper')->create([
            'first_name' => 'Junior',
            'last_name' => 'Cruise',
        ]);
        $guardian = factory(Person::class)->states('guardian')->create([
            'first_name' => 'Tom',
            'last_name' => 'Cruise',
        ]);
        $guardianProfile = factory(PersonProfile::class)->create([
            'person_id' => $guardian->id,
            'cell_phone' => '0123456789',
        ]);

        $camper->guardians()->attach($guardian->id);

        $check = factory(Check::class)->create([
            'meetup_id' => $meetup,
            'person_id' => $camper,
            'time' => now(),
            'notes' => 'Checked In',
            'in_out' => 1,
        ]);

        Notification::assertSentTo(
            $guardianProfile,
            function (CamperCheckedInToMeetup $notification, $channels) use ($camper, $meetup) {
                return $notification->meetup->id === $meetup->id
                    && $notification->camper->id === $camper->id
                    && $channels === [TwilioChannel::class];
            }
        );
    }

    /** @test */
    public function it_can_generate_notification_for_a_checked_out()
    {
        Notification::fake();
        TestTime::freeze();

        $meetup = factory(Meetup::class)->create([
            'start' => now(),
            'end' => now()->addHours(2),
        ]);
        $camper = factory(Person::class)->states('camper')->create([
            'first_name' => 'Junior',
            'last_name' => 'Cruise',
        ]);
        $guardian = factory(Person::class)->states('guardian')->create([
            'first_name' => 'Tom',
            'last_name' => 'Cruise',
        ]);
        $guardianProfile = factory(PersonProfile::class)->create([
            'person_id' => $guardian->id,
            'cell_phone' => '0123456789',
        ]);

        $camper->guardians()->attach($guardian->id);

        $check = factory(Check::class)->create([
            'meetup_id' => $meetup,
            'person_id' => $camper,
            'time' => now(),
            'notes' => 'Checked Out',
            'in_out' => 0,
        ]);

        Notification::assertSentTo(
            $guardianProfile,
            function (CamperCheckedOutFromMeetup $notification, $channels) use ($camper, $meetup) {
                return $notification->meetup->id === $meetup->id
                    && $notification->camper->id === $camper->id
                    && $channels === [TwilioChannel::class];
            }
        );
    }

    /** @test */
    public function it_can_generate_notification_for_a_rechecked_in()
    {
        Notification::fake();
        TestTime::freeze();

        $meetup = factory(Meetup::class)->create([
            'start' => now(),
            'end' => now()->addHours(2),
        ]);
        $camper = factory(Person::class)->states('camper')->create([
            'first_name' => 'Junior',
            'last_name' => 'Cruise',
        ]);
        $guardian = factory(Person::class)->states('guardian')->create([
            'first_name' => 'Tom',
            'last_name' => 'Cruise',
        ]);
        $guardianProfile = factory(PersonProfile::class)->create([
            'person_id' => $guardian->id,
            'cell_phone' => '0123456789',
        ]);

        $camper->guardians()->attach($guardian->id);

        $check = $meetup->checkIn($camper, now(), 'Initial');

        Notification::assertNothingSent();

        $check->in_out = 1;
        $check->save();

        Notification::assertSentTo(
            $guardianProfile,
            function (CamperCheckedInToMeetup $notification, $channels) use ($camper, $meetup) {
                return $notification->meetup->id === $meetup->id
                    && $notification->camper->id === $camper->id
                    && $channels === [TwilioChannel::class];
            }
        );
    }
}
