<?php

return [
    'checked-in' => ':camper has checked-in to :meetup (:program)',
    'checked-out' => ':camper has checked-out from :meetup (:program)',
];
