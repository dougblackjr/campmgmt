<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    {{-- <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet"> --}}
</head>
<body class="bg-grey-lightest font-sans leading-normal tracking-normal">
    <nav id="header" class="bg-white fixed w-full z-10 pin-t shadow">
        <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 md:pb-0">
            <div class="w-1/2 pl-2 md:pl-0">
                <a class="text-black text-base xl:text-xl no-underline hover:no-underline font-bold" href="/">
                    <i class="fas fa-sun text-orange-dark pr-3"></i> CampMGMT
                </a>
            </div>
            @include('partials/nav')
            {{-- SECONDARY NAV --}}
            <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-white z-20" id="nav-content">
                <ul class="list-reset lg:flex flex-1 items-center px-4 md:px-0">
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app" class="block py-1 md:py-3 pl-1 align-middle text-orange-dark no-underline hover:text-black border-b-2 {{ Route::is('home') ? 'border-orange-dark' : 'border-white' }} hover:border-orange-dark">
                            <i class="fas fa-home fa-fw mr-3 {{ Route::is('home') ? 'text-orange-dark' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Home</span>
                        </a>
                    </li>
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app/program" class="block py-1 md:py-3 pl-1 align-middle text-grey no-underline hover:text-black border-b-2 {{ Route::is('program') ? 'border-green' : 'border-white' }} hover:border-green">
                            <i class="fas fa-chart-area fa-fw mr-3 {{ Route::is('program') ? 'text-green' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Program</span>
                        </a>
                    </li>
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app/person" class="block py-1 md:py-3 pl-1 align-middle text-grey no-underline hover:text-black border-b-2 {{ Route::is('people') ? 'border-pink' : 'border-white' }} hover:border-pink">
                            <i class="fas fa-tasks fa-fw mr-3 {{ Route::is('people') ? 'text-pink' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">People</span>
                        </a>
                    </li>
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app/team" class="block py-1 md:py-3 pl-1 align-middle text-grey no-underline hover:text-black border-b-2 {{ Route::is('team') ? 'border-purple' : 'border-white' }} hover:border-purple">
                            <i class="fa fa-envelope fa-fw mr-3 {{ Route::is('team') ? 'text-purple' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Teams</span>
                        </a>
                    </li>
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app/meetup" class="block py-1 md:py-3 pl-1 align-middle text-grey no-underline hover:text-black border-b-2 {{ Route::is('meetup') ? 'border-green' : 'border-white' }} hover:border-green">
                            <i class="fas fa-chart-area fa-fw mr-3 {{ Route::is('meetup') ? 'text-green' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Meetups</span>
                        </a>
                    </li>
                    <li class="mr-6 my-2 md:my-0">
                        <a href="/app/account" class="block py-1 md:py-3 pl-1 align-middle text-grey no-underline hover:text-black border-b-2 {{ Route::is('account') ? 'border-red' : 'border-white' }} hover:border-red">
                            <i class="fa fa-wallet fa-fw mr-3 {{ Route::is('account') ? 'text-red' : '' }}"></i><span class="pb-1 md:pb-0 text-sm">Account</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Container-->
    <main class="container w-full mx-auto pt-20" id="app">
        @yield('content')
    </main>
    <!--/container-->
    @include('partials/footer')
    @include('partials/scripts')
</body>
</html>
