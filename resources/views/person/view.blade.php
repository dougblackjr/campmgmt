@extends('layouts.app')

@section('content')
   <person-view :inperson='@json($person)' :user='@json($user)'></person-view>
@endsection
