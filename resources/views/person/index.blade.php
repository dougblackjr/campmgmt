@extends('layouts.app')

@section('content')
   <person-dashboard :inprogram='@json($program)' :user='@json($user)'></person-dashboard>
@endsection
