@extends('layouts.app')

@section('content')
   <account-dashboard :user='@json($user)'></account-dashboard>
@endsection
