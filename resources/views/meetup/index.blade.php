@extends('layouts.app')

@section('content')
   <meetup-dashboard :user='@json($user)'></meetup-dashboard>
@endsection
