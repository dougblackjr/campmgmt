@extends('layouts.app')

@section('content')
   <meetup-view :inmeetup='@json($meetup)' :user='@json($user)'></meetup-view>
@endsection
