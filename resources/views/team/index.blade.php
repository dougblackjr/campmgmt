@extends('layouts.app')

@section('content')
   <team-overview :inprogram='@json($program)' :user='@json($user)'></team-overview>
@endsection
