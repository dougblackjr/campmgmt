@extends('layouts.app')

@section('content')
   <team-view :inteam='@json($team)' :user='@json($user)'></team-view>
@endsection
