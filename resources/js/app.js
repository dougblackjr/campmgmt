require('./bootstrap');

window.Vue = require('vue');

// Event hub
window.Event = new Vue();

// Notifications
import VueAWN from "vue-awesome-notifications"

// Set global options
let globalOptions =  {}

Vue.use(VueAWN, globalOptions)

// Global components
import VModal from 'vue-js-modal'
Vue.use(VModal, {
	dynamic: true,
	dynamicDefaults: {
		clickToClose: true
	},
	injectModalsContainer: true
})

import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.min.css'
Vue.use(Datetime)

// Components
Vue.component('dashboard', require('./components/Dashboard').default)
Vue.component('account-dashboard', require('./components/account/AccountDashboard').default)
Vue.component('program-dashboard', require('./components/program/ProgramDashboard').default)
Vue.component('team-dashboard', require('./components/team/TeamDashboard').default)
Vue.component('team-overview', require('./components/program/TeamOverview').default)
Vue.component('team-view', require('./components/team/TeamView').default)
Vue.component('meetup-dashboard', require('./components/meetup/MeetupDashboard').default)
Vue.component('person-dashboard', require('./components/person/PersonDashboard').default)
Vue.component('person-view', require('./components/person/PersonView').default)
Vue.component('meetup-view', require('./components/meetup/MeetupView').default)
Vue.component('badge', require('./components/ui/Badge').default)
Vue.component('confirm', require('./components/ui/Confirm').default)

// Filters
Vue.filter('toTitleCase', function(value) {

	let str = value.toLowerCase().split(' ');

	for (var i = 0; i < str.length; i++) {

		str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);

	}

	return str.join(' ');

})

const app = new Vue({
    el: '#app',
});
