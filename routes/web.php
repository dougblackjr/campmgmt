<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/app');

Auth::routes();

Route::group(['prefix' => 'app', 'middleware' => 'auth'], function() {

	// Home
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/dashboard', 'HomeController@dashboard');

	// Media
	Route::post('media', 'MediaController@put');

	// Account
	Route::group(['prefix' => 'account'], function() {

		Route::get('/', 'AccountController@index')->name('account');

		Route::get('/get', 'AccountController@show');

		Route::post('/update-account', 'AccountController@updateAccount');
		Route::post('/add-program', 'AccountController@addProgram');

	});

	// Program
	Route::group(['prefix' => 'program'], function() {

		Route::get('/', 'ProgramController@index')->name('program');

		Route::get('/get', 'ProgramController@show');

		Route::post('/edit', 'ProgramController@edit');

	});

	// Team
	Route::group(['prefix' => 'team'], function() {

		Route::get('/', 'TeamController@index')->name('team');

		Route::get('/get', 'TeamController@show');

		Route::get('/{id}', 'TeamController@get');

		Route::post('/add', 'TeamController@store');

	});

	// Meetup
	Route::group(['prefix' => 'meetup'], function() {

		Route::get('/', 'MeetupController@index')->name('meetup');

		Route::get('/get', 'MeetupController@query');

		Route::get('/{meetup}', 'MeetupController@show');

		Route::get('/history/{meetup}', 'MeetupController@showWithAllChecks');

		Route::post('/add', 'MeetupController@store');

		Route::get('/start/{id}', 'MeetupController@startMeetup');

	});

	Route::group(['prefix' => 'check'], function() {

		Route::post('/{check}', 'CheckController@update');

	});

	// Person
	Route::group(['prefix' => 'person'], function() {

		Route::get('/', 'PersonController@index');

		Route::get('/get', 'PersonController@get');

		Route::get('/get/{id}', 'PersonController@show');

		Route::get('/get-type/{type}', 'PersonController@getType');

		Route::post('/add', 'PersonController@store');

		Route::post('/add-with-type', 'PersonController@storeWithRole');

		Route::post('/edit', 'PersonController@update');

		Route::delete('/delete/{id}', 'PersonController@remove');

		Route::get('/view/{id}', 'PersonController@showView');

		Route::post('/remove-from-team', 'TeamController@removeFromTeam');

	});

});
